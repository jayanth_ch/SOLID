# **SOLID**

**SOLID** is an acronym for 5 important design principles when doing OOP (Object-Oriented Programming).

These 5 principles were introduced by [Robert C. Martin](https://en.wikipedia.org/wiki/Robert_C._Martin) (Uncle Bob), in his 2000 paper
[Design Principles and Design Patterns](https://web.archive.org/web/20150906155800/http://www.objectmentor.com/resources/articles/Principles_and_Patterns.pdf "Design principles & Design pattern").

>## Solid stands for

* **S**: Single responsibility principle
* **O** : Open/closed principle
* **L** : Liskov substitution principle
* **I** : Interface segregation principle
* **D** : Dependency inversion principle

----

### **Single Responsibility Principle** (SRP)

Each software module should have one and the only reason to change means that the module should have only one job. Whenever the classes perform too many things, they often end-up in lots of coupling things.\
For that, classes should be encapsulated to perform a single task.

According to the SRP, a class should have only one job. If it has more responsibilities it becomes coupled. The below code violates the SRP. The animal class takes two responsibilities of properties and databases.

```python
class Animal:
    def __init__(self, name: str):
        self.name = name
    
    def get_name(self) -> str:
        pass

    def save(self, animal: Animal):
        pass
```

The below code satisfies the SRP. The responsibilities of animal properties and animal databases are divided into two classes.

``` python
class Animal:
    def __init__(self, name: str):
        self.name = name
    
    def get_name(self) -> str:
        pass


class AnimalDB:
    def get_animal(self, id) -> Animal:
        pass

    def save(self, animal: Animal):
        pass
 ```

### **Open/closed Principle** (OCP)

[Bertrand Meyer](https://en.wikipedia.org/wiki/Bertrand_Meyer) is generally credited for having originated the term open–closed principle, which appeared in his 1988 book Object-Oriented Software Construction.

Software entities
(modules, classes, functions etc…) should be open for extensions and closed for modifications.

* A module will be said to be open if it is still available for extension. For example, it should be possible to add fields to the data structures it contains, or new elements to the set of functions it performs.
* A module will be said to be closed if it is available for use by other modules. This assumes that the module has been given a well-defined, stable description (the interface in the sense of information hiding).

We have initially written a code to give a discount to customers after that we need to give a special discount to the VIPs. For we are modifying the code like below by adding another if statement. It violates the OCP.

```python
class Discount:
  def __init__(self, customer, price):
      self.customer = customer
      self.price = price
  def give_discount(self):
      if self.customer == 'fav':
          return self.price * 0.2
      if self.customer == 'vip':
          return self.price * 0.4

```

Instead of modifying, we are adding an extension by adding a VIP discount class as shown below.

```python
class Discount:
    def __init__(self, customer, price):
      self.customer = customer
      self.price = price
    def get_discount(self):
      return self.price * 0.2
class VIPDiscount(Discount):
    def get_discount(self):
      return super().get_discount() * 2

```

### **Liskov substitution Principle** (LSP)

LSP was initially introduced by [Barbara Liskov](https://en.wikipedia.org/wiki/Barbara_Liskov)
in 1987. It provides the guidelines for using the inheritance properly for any object-oriented programming languages such are python, c#, or Java.\
Let φ(x) be a property provable about objects x of type T. Then φ(y) should be true for objects y of type S where S is a subtype of T.
More formally, this is the original definition (LISKOV 01) of Liskov’s substitution principle: if S is a subtype of T, then objects of type T may be replaced by objects of type S, without breaking the program.

Liskov's notion of a behavioral subtype defines a notion of substitutability for objects; that is, if S is a subtype of T, then objects of type T in a program may be replaced with objects of type S without altering any of the desirable properties of that program.

```python
class User():
  def __init__(self, color, board):
    create_pieces()
    self.color = color
    self.board = board
  def move(self, piece:Piece, position:int):
      piece.move(position)
      chessmate_check()
  board = ChessBoard()
  user_white = User("white", board)
  user_black = User("black", board)
  pieces = user_white.pieces
  horse = helper.getHorse(user_white, 1)
  user.move(horse)
```

### **Interface segregation Principle** (ISP)

ISP states that: A client should never be forced to implement an interface that it doesn’t use, or clients shouldn’t be forced to depend on methods they do not use.

```python
class Ishape:
    def draw(self):
        raise NotImplementedError

class Circle(IShape):
    def draw(self):
        pass

class Square(IShape):
    def draw(self):
        pass

class Rectangle(IShape):
    def draw(self):
        pass
```

### **Dependency inversion Principle** (DIP)

DIP states that: Entities must depend on abstractions, not on concretions. It states that the high-level module must not depend on the low-level module, but should depend on abstractions.

In the bad example here, the WeatherTracker depends on the low-level details of the different notification systems (a phone, an emailer, etc.). These should instead be depending on some abstraction.

## **References**

1. <https://en.wikipedia.org/wiki/SOLID#:~:text=In%20object%2Doriented%20computer%20programming,Martin>

1. <https://www.digitalocean.com/community/conceptual_articles/s-o-l-i-d-the-first-five-principles-of-object-oriented-design#interface-segregation-principle>

1. <https://hackernoon.com/solid-principles-simple-and-easy-explanation-f57d86c47a7f>

1. <http://butunclebob.com/ArticleS.UncleBob.PrinciplesOfOod>
